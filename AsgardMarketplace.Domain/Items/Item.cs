﻿using AsgardMarketplace.Domain.Orders;

namespace AsgardMarketplace.Domain.Items
{
    public class Item : IEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public decimal Price { get; set; }
        public Order Order { get; set; }
    }
}
