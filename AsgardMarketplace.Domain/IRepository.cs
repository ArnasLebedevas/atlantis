﻿using System.Threading.Tasks;

namespace AsgardMarketplace.Domain
{
    public interface IRepository<TEntity>
    {
        void Add(TEntity entity);
        void Remove(TEntity entity);
        Task<TEntity> GetAsync(int id);
        Task SaveAsync();
    }
}
