﻿using AsgardMarketplace.Domain.Orders;
using System;

namespace AsgardMarketplace.Domain.Payments
{
    public class Payment : IEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public DateTime Date { get; set; }
        public bool IsPaid { get; set; }
        public Order Order { get; set; }
        public int OrderID { get; set; }
    }
}
