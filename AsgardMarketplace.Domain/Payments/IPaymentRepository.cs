﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Domain.Payments
{
    public interface IPaymentRepository : IRepository<Payment>
    {
        Task<List<Payment>> GetAllPaymentsAsync();
    }
}
