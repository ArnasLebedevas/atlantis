﻿namespace AsgardMarketplace.Domain
{
    public interface IEntity
    {
        int ID { get; }
    }
}
