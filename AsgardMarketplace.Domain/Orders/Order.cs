﻿using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Payments;
using System;

namespace AsgardMarketplace.Domain.Orders
{
    public class Order : IEntity
    {
        public int ID { get; set; }
        public int Quantity { get; set; }
        public DateTime From { get; set; }
        public bool IsOrdered { get; set; }
        public bool IsCompleted { get; set; }
        public Item Item { get; set; }
        public int ItemID { get; set; }
        public Payment Payment { get; set; }
    }
}
