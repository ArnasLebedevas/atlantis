﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Domain.Orders
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<List<Order>> GetAllOrdersAsync();
    }
}
