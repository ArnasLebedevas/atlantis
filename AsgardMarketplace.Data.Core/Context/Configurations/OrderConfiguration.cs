﻿using AsgardMarketplace.Domain.Orders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AsgardMarketplace.Data.Core.Context.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder
                .HasOne(o => o.Item)
                .WithOne(b => b.Order)
                .HasForeignKey<Order>(o => o.ItemID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
