﻿using AsgardMarketplace.Domain.Payments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AsgardMarketplace.Data.Core.Context.Configurations
{
    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder
                .HasOne(o => o.Order)
                .WithOne(p => p.Payment)
                .HasForeignKey<Payment>(o => o.OrderID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
