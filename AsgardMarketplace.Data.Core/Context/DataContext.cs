﻿using AsgardMarketplace.Data.Core.Context.Configurations;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Domain.Payments;
using Microsoft.EntityFrameworkCore;

namespace AsgardMarketplace.Data.Core.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        public DbSet<Item> Items { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Payment> Payments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new PaymentConfiguration());
        }
    }
}
