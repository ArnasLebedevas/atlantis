﻿using AsgardMarketplace.Domain;
using AsgardMarketplace.Models.Base.Infrastructure;

namespace AsgardMarketplace.Data.Core.Context
{
    public class DataRepository<TEntity> : Repository<TEntity>
        where TEntity : class, IEntity
    {
        public DataRepository(IWriteContext context) : base(context)
        { }
    }
}
