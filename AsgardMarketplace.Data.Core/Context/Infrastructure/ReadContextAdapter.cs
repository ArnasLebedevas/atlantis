﻿using AsgardMarketplace.Models.Base.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Core.Context.Infrastructure
{
    public class ReadContextAdapter : IReadContext
    {
        private readonly DataContext context;

        public ReadContextAdapter(DataContext context)
        {
            this.context = context;
        }

        public Task<TResult> FirstOrDefaultAsync<TResult>(IQueryable<TResult> queryable)
        {
            return queryable.FirstOrDefaultAsync();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return context.Set<T>();
        }

        public Task<List<TResult>> ToListAsync<TResult>(IQueryable<TResult> queryable)
        {
            return queryable.ToListAsync();
        }
    }
}
