﻿using AsgardMarketplace.Models.Base.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Core.Context.Infrastructure
{
    public class WriteContextAdapter : IWriteContext
    {
        private readonly DataContext context;

        public WriteContextAdapter(DataContext context)
        {
            this.context = context;
        }

        public void Add<TEntity>(TEntity entity)
        {
            context.Add(entity);
        }

        public Task<TEntity> FirstOrDefaultAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class
        {
            return context.Set<TEntity>().FirstOrDefaultAsync(where);
        }

        public void Remove<TEntity>(TEntity entity)
        {
            if (entity == null)
                return;

            context.Remove(entity);
        }

        public Task SaveChangesAsync()
        {
            return context.SaveChangesAsync();
        }

        public Task<List<TEntity>> ToListAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class
        {
            return context.Set<TEntity>().Where(where).ToListAsync();
        }
    }
}
