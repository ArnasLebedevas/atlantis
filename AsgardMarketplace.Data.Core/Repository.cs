﻿using AsgardMarketplace.Domain;
using AsgardMarketplace.Models.Base.Infrastructure;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Core
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected IWriteContext context;

        public Repository(IWriteContext context)
        {
            this.context = context;
        }

        public void Add(TEntity entity)
        {
            context.Add(entity);
        }

        public Task<TEntity> GetAsync(int id)
        {
            return context.FirstOrDefaultAsync<TEntity>(o => o.ID == id);
        }

        public void Remove(TEntity entity)
        {
            context.Remove(entity);
        }

        public Task SaveAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
