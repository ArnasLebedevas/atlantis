﻿using AsgardMarketplace.Data.Core.Context;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Core.Orders
{
    public class OrderRepository : DataRepository<Order>, IOrderRepository
    {
        public OrderRepository(IWriteContext context) : base(context)
        { }

        public async Task<List<Order>> GetAllOrdersAsync()
        {
            return await context.ToListAsync<Order>(o => EF.Functions.DateDiffHour(o.From, DateTime.Now) >= 2);
        }
    }
}
