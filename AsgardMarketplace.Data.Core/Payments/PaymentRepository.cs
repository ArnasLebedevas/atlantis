﻿using AsgardMarketplace.Data.Core.Context;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Core.Payments
{
    public class PaymentRepository : DataRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(IWriteContext context) : base(context)
        { }

        public async Task<List<Payment>> GetAllPaymentsAsync()
        {
            return await context.ToListAsync<Payment>(p => p.IsPaid && EF.Functions.DateDiffDay(p.Date, DateTime.Now) >= 1);
        }
    }
}
