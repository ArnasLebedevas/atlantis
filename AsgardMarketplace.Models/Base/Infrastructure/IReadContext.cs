﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base.Infrastructure
{
    public interface IReadContext
    {
        IQueryable<T> Query<T>() where T : class;

        Task<TResult> FirstOrDefaultAsync<TResult>(IQueryable<TResult> queryable);

        Task<List<TResult>> ToListAsync<TResult>(IQueryable<TResult> queryable);
    }
}
