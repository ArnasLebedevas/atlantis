﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base.Infrastructure
{
    public interface IWriteContext
    {
        void Add<TEntity>(TEntity entity);
        void Remove<TEntity>(TEntity entity);
        Task<TEntity> FirstOrDefaultAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class;
        Task<List<TEntity>> ToListAsync<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class;
        Task SaveChangesAsync();
    }
}
