﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base.Queries
{
    public interface IQuery<TParam, TResult>
    {
        Task<List<TResult>> ToList(TParam param);

        Task<TResult> FirstOrDefault(TParam param);
    }
}
