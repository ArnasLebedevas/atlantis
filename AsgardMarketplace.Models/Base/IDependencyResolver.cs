﻿namespace AsgardMarketplace.Models.Base
{
    public interface IDependencyResolver
    {
        T Resolve<T>();
    }
}
