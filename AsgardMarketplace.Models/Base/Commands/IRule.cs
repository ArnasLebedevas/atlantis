﻿using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base.Commands
{
    public interface IRule<TCommand>   
    {
        Task<ICommandResult> Validate(TCommand command);
    }
}
