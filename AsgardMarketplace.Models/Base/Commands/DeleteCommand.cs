﻿namespace AsgardMarketplace.Models.Base.Commands
{
    public class DeleteCommand<TModel> : Command
    {
        public DeleteCommand()
        { }

        public DeleteCommand(int id)
        {
            ID = id;
        }

        public int ID { get; set; }
    }
}
