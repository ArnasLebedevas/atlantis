﻿using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base
{
    public interface IQueryHandler<TParam, TResult>
    {
        Task<TResult> Execute(TParam param);
    }
}
