﻿namespace AsgardMarketplace.Models.Base.Params
{
    public class DefaultParam
    {
        public DefaultParam()
        { }

        public DefaultParam(int masterID)
        {
            MasterID = masterID;
        }

        public int MasterID { get; set; }
    }
}
