﻿using System.Threading.Tasks;

namespace AsgardMarketplace.Models.Base
{
    public interface ICommandHandler<TCommand>
    {
        Task<ICommandResult> Handle(TCommand command);
    }
}
