﻿using AsgardMarketplace.Models.Items;
using System;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models.Orders
{
    public class OrderModel : IEntityModel
    {
        public int ID { get; set; }
        [Range(1, 10)]
        [Required]
        public int Quantity { get; set; }
        public ItemModel Item { get; set; }
        public int ItemID { get; set; }
    }
}
