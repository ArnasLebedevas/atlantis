﻿namespace AsgardMarketplace.Models.Orders
{
    public class OrderListModel : IEntityModel
    {
        public int ID { get; set; }
        public int Quantity { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImagePath { get; set; }
        public decimal Price { get; set; }
    }
}
