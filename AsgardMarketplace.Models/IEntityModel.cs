﻿namespace AsgardMarketplace.Models
{
    public interface IEntityModel
    {
        int ID { get; set; }
    }
}
