﻿using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models.Items
{
    public class ItemModel : IEntityModel
    {
        public int ID { get; set; }
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        [Required]
        public string Description { get; set; }
        public string ImagePath { get; set; }
        [Range(0, 999.99)]
        [Required]
        public decimal Price { get; set; }
    }
}
