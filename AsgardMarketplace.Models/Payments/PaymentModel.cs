﻿using AsgardMarketplace.Models.Orders;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models.Payments
{
    public class PaymentModel : IEntityModel
    {
        public int ID { get; set; }
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(30)]
        [Required]
        public string City { get; set; }
        [StringLength(50)]
        [Required]
        public string Address { get; set; }
        [StringLength(20)]
        [Required]
        public string PostCode { get; set; }
        [StringLength(30)]
        [Required]
        public string Country { get; set; }
        public OrderModel Order { get; set; }
        public int OrderID { get; set; }
    }
}
