﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Items;
using AsgardMarketplace.Models.Orders;
using System.Linq;

namespace AsgardMarketplace.Data.Orders
{
    public class OrderQuery : BaseQuery<int, OrderModel>
    {
        public OrderQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<OrderModel> Query(int param)
        {
            return
                from o in Query<Order>()
                where o.ID == param
                join i in Query<Item>()
                on o.ItemID equals i.ID into oi
                from i in oi.DefaultIfEmpty()
                select new OrderModel
                {
                    ID = o.ID,
                    Quantity = o.Quantity,
                    Item = new ItemModel
                    {
                        ID = i.ID,
                        Name = i.Name,
                        Description = i.Description,
                        ImagePath = i.ImagePath,
                        Price = i.Price
                    }
                };
        }
    }
}
