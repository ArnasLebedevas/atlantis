﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Orders;
using System.Linq;

namespace AsgardMarketplace.Data.Orders
{
    public class OrderListQuery : BaseQuery<string, OrderListModel>
    {
        public OrderListQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<OrderListModel> Query(string param)
        {
            return
               from o in Query<Order>()
               join i in Query<Item>()
               on o.ItemID equals i.ID into oi
               from i in oi.DefaultIfEmpty()
               select new OrderListModel
               {
                   ID = o.ID,
                   Quantity = o.Quantity,
                   ItemName = i.Name,
                   ItemDescription = i.Description,
                   ItemImagePath = i.ImagePath,
                   Price = i.Price
               };
        }
    }
}
