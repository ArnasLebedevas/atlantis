﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Items;
using System.Linq;

namespace AsgardMarketplace.Data.Items
{
    public class ItemListQuery : BaseQuery<string, ItemListModel>
    {
        public ItemListQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<ItemListModel> Query(string param)
        {
            return
                from i in Query<Item>()
                select new ItemListModel
                {
                    ID = i.ID,
                    Name = i.Name,
                    Description = i.Description,
                    ImagePath = i.ImagePath,
                    Price = i.Price
                };
        }
    }
}
