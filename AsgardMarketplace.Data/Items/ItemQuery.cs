﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Items;
using System.Linq;

namespace AsgardMarketplace.Data.Items
{
    public class ItemQuery : BaseQuery<int, ItemModel>
    {
        public ItemQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<ItemModel> Query(int param)
        {
            return
                from i in Query<Item>()
                where i.ID == param
                select new ItemModel
                {
                    ID = i.ID,
                    Name = i.Name,
                    Description = i.Description,
                    ImagePath = i.ImagePath,
                    Price = i.Price
                };
        }
    }
}
