﻿using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Base.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsgardMarketplace.Data.Base
{
    public abstract class BaseQuery<TParam, TResult> : IQuery<TParam, TResult>
    {
        protected IReadContext context;

        public BaseQuery(IReadContext context)
        {
            this.context = context;
        }

        protected abstract IQueryable<TResult> Query(TParam param);

        protected IQueryable<T> Query<T>() where T : class
        {
            return context.Query<T>();
        }

        public Task<TResult> FirstOrDefault(TParam param)
        {
            var query = Query(param);

            return context.FirstOrDefaultAsync(query);
        }

        public Task<List<TResult>> ToList(TParam param)
        {
            return context.ToListAsync(Query(param));
        }
    }
}
