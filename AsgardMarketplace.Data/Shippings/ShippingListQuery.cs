﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Shippings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace AsgardMarketplace.Data.Shippings
{
    public class ShippingListQuery : BaseQuery<ShippingItemParam, ShippingItemListModel>
    {
        public ShippingListQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<ShippingItemListModel> Query(ShippingItemParam param)
        {
            return
               from p in Query<Payment>()
               where p.IsPaid && EF.Functions.DateDiffDay(p.Date, DateTime.Now) >= 1
               join i in Query<Order>()
               on p.OrderID equals i.ID into oi
               from i in oi.DefaultIfEmpty()
               join ii in Query<Item>()
               on p.Order.ItemID equals ii.ID into iio
               from ii in iio.DefaultIfEmpty()
               select new ShippingItemListModel
               {
                   Name = p.Name,
                   City = p.City,
                   Country = p.Country,
                   Address = p.Address,
                   PostCode = p.PostCode,
                   Quantity = i.Quantity,
                   ItemName = ii.Name,
                   ItemDescription = ii.Description,
                   ItemImagePath = ii.ImagePath,
                   Price = ii.Price
               };
        }
    }
}
