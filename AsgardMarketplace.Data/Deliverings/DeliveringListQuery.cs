﻿using AsgardMarketplace.Data.Base;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base.Infrastructure;
using AsgardMarketplace.Models.Deliverings;
using System.Linq;

namespace AsgardMarketplace.Data.Deliverings
{
    public class DeliveringListQuery : BaseQuery<DeliveringItemParam, DeliveringItemListModel>
    {
        public DeliveringListQuery(IReadContext context) : base(context)
        { }

        protected override IQueryable<DeliveringItemListModel> Query(DeliveringItemParam param)
        {
            return
                from p in Query<Payment>()
                join o in Query<Order>()
                on p.OrderID equals o.ID into oi
                from i in oi.DefaultIfEmpty()
                join ii in Query<Item>()
                on p.Order.ItemID equals ii.ID into iio
                from ii in iio.DefaultIfEmpty()
                where i.IsCompleted
                select new DeliveringItemListModel
                {
                    Name = p.Name,
                    City = p.City,
                    Country = p.Country,
                    Address = p.Address,
                    PostCode = p.PostCode,
                    Quantity = i.Quantity,
                    ItemName = ii.Name,
                    ItemDescription = ii.Description,
                    ItemImagePath = ii.ImagePath,
                    Price = ii.Price
                };
        }
    }
}
