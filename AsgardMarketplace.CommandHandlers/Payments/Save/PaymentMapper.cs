﻿using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Payments;
using System;

namespace AsgardMarketplace.CommandHandlers.Payments.Save
{
    public class PaymentMapper : IMapper<PaymentModel, Payment>
    {
        public void Map(PaymentModel model, Payment entity)
        {
            entity.ID = model.ID;
            entity.Name = model.Name;
            entity.Country = model.Country;
            entity.City = model.City;
            entity.Address = model.Address;
            entity.PostCode = model.PostCode;
            entity.Date = DateTime.Now;
            entity.IsPaid = true;
            entity.OrderID = model.OrderID;
        }
    }
}
