﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Payments;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Payments.Save
{
    public class PaymentSaveHandler : SaveHandler<PaymentModel, Payment>
    {
        private readonly IOrderRepository orderRepository;

        public PaymentSaveHandler(IPaymentRepository paymentRepository, IMapper<PaymentModel, Payment> mapper, IOrderRepository orderRepository)
            : base(paymentRepository, mapper)
        {
            this.orderRepository = orderRepository;
        }

        public override async Task<ICommandResult> Handle(PaymentModel model)
        {
            var entity = await MapEntity(model);

            var order = await orderRepository.GetAsync(model.OrderID);

            if (order == null || !order.IsOrdered)
                return new FailureResult("This item is not ordered");
             
            if (entity == null)
                return new FailureResult("Item is already purchased");

            if (model.ID == 0)
                repository.Add(entity);

            await repository.SaveAsync();

            return new SuccessResult();
        }

        protected override async Task<Payment> MapEntity(PaymentModel model)
        {
            Payment entity;

            if (model.ID == 0)
                entity = new Payment();
            else
            {
                entity = await repository.GetAsync(model.ID);

                if (entity.IsPaid)
                    return null;
            }

            mapper.Map(model, entity);

            return entity;
        }
    }
}
