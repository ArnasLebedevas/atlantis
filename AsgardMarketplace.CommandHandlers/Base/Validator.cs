﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Base
{
    public class Validator : IValidator
    {
        public async Task<ICommandResult> Validate<TCommand>(TCommand command, IEnumerable<IRule<TCommand>> rules)
        {
            var failResult = new FailureResult();

            foreach (var rule in rules.ToArray())
            {
                var ruleResult = await rule.Validate(command);

                if (!ruleResult.IsValid)
                {
                    failResult.Merge(ruleResult);
                }
            }

            if (!failResult.IsValid)
            {
                return failResult;
            }

            return new SuccessResult();
        }
    }
}
