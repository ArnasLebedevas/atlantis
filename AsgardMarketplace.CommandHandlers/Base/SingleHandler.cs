﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Queries;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Base
{
    public class SingleHandler<TModel> : IQueryHandler<int, TModel>
    {
        private readonly IQuery<int, TModel> query;

        public SingleHandler(IQuery<int, TModel> query)
        {
            this.query = query;
        }

        public virtual Task<TModel> Execute(int id)
        {
            var model = query.FirstOrDefault(id);

            return model;
        }
    }

    public class SingleHandler<TParam, TResult> : IQueryHandler<TParam, TResult>
        where TResult : new()
    {
        private readonly IQuery<TParam, TResult> query;

        public SingleHandler(IQuery<TParam, TResult> query)
        {
            this.query = query;
        }

        public virtual async Task<TResult> Execute(TParam param)
        {
            var model = await query.FirstOrDefault(param);

            return model;
        }
    }
}
