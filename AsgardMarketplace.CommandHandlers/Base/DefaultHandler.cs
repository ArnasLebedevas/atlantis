﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Params;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Base
{
    public class DefaultHandler<TResult> : DefaultHandler<DefaultParam, TResult>
        where TResult : new()
    {
    }

    public class DefaultHandler<TParam, TResult> : IQueryHandler<TParam, TResult>
        where TResult : new()
    {
        public virtual Task<TResult> Execute(TParam param)
        {
            var model = Task.FromResult(new TResult());

            return model;
        }
    }
}
