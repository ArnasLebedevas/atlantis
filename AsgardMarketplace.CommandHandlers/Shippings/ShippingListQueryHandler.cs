﻿using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Queries;
using AsgardMarketplace.Models.Shippings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Shippings
{
    public class ShippingListQueryHandler : IQueryHandler<ShippingItemParam, List<ShippingItemListModel>>
    {
        private readonly IQuery<ShippingItemParam, ShippingItemListModel> query;
        private readonly IPaymentRepository paymentRepository;
        private readonly IOrderRepository orderRepository;

        public ShippingListQueryHandler(IQuery<ShippingItemParam, ShippingItemListModel> query, IPaymentRepository paymentRepository,
            IOrderRepository orderRepository)
        {
            this.query = query;
            this.paymentRepository = paymentRepository;
            this.orderRepository = orderRepository;
        }

        public async Task<List<ShippingItemListModel>> Execute(ShippingItemParam param)
        {
            var list = await query.ToList(param);

            if (list != null)
            {
                var payments = await paymentRepository.GetAllPaymentsAsync();

                foreach (var payment in payments)
                {
                    var order = await orderRepository.GetAsync(payment.OrderID);

                    if (order != null)
                        order.IsCompleted = true;
                }

                await paymentRepository.SaveAsync();
            }

            return list;
        }
    }
}
