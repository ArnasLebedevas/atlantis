﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Orders;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Orders.Save
{
    public class OrderSaveHandler : SaveHandler<OrderModel, Order>
    {
        public OrderSaveHandler(IOrderRepository orderRepository, IMapper<OrderModel, Order> mapper)
            : base(orderRepository, mapper)
        { }

        public override async Task<ICommandResult> Handle(OrderModel model)
        {
            var entity = await MapEntity(model);

            if (entity == null)
                return new FailureResult("This order is already ordered");

            if (model.ID == 0)
                repository.Add(entity);

            await repository.SaveAsync();

            return new SuccessResult();
        }

        protected override async Task<Order> MapEntity(OrderModel model)
        {
            Order entity;

            if (model.ID == 0)
                entity = new Order();
            else
            {
                entity = await repository.GetAsync(model.ID);

                if (entity.IsOrdered)
                    return null;
            }

            mapper.Map(model, entity);

            return entity;
        }
    }
}
