﻿using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Orders;
using System;

namespace AsgardMarketplace.CommandHandlers.Orders.Save
{
    public class OrderMapper : IMapper<OrderModel, Order>
    {
        public void Map(OrderModel model, Order entity)
        {
            entity.ID = model.ID;
            entity.From = DateTime.Now;
            entity.IsOrdered = true;
            entity.Quantity = model.Quantity;
            entity.ItemID = model.ItemID;
        }
    }
}
