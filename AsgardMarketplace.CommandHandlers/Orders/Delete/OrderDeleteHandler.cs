﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Orders;

namespace AsgardMarketplace.CommandHandlers.Orders.Delete
{
    public class OrderDeleteHandler : DeleteHandler<OrderModel, Order>
    {
        public OrderDeleteHandler(IOrderRepository repository) : base(repository)
        { }
    }
}
