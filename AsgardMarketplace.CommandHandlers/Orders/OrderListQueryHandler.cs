﻿using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Queries;
using AsgardMarketplace.Models.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Orders
{
    public class OrderListQueryHandler : IQueryHandler<string, List<OrderListModel>>
    {
        private readonly IQuery<string, OrderListModel> query;
        private readonly IOrderRepository orderRepository;

        public OrderListQueryHandler(IQuery<string, OrderListModel> query, IOrderRepository orderRepository)
        {
            this.query = query;
            this.orderRepository = orderRepository;
        }

        public async Task<List<OrderListModel>> Execute(string param)
        {
            var orders = await orderRepository.GetAllOrdersAsync();

            if (orders != null)
                foreach (var order in orders)
                    orderRepository.Remove(order);

            await orderRepository.SaveAsync();

            return await query.ToList(param);
        }
    }
}
