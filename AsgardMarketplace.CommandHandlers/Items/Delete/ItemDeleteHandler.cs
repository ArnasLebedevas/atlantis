﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Domain;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Models.Items;

namespace AsgardMarketplace.CommandHandlers.Items.Delete
{
    public class ItemDeleteHandler : DeleteHandler<ItemModel, Item>
    {
        public ItemDeleteHandler(IRepository<Item> repository) : base(repository)
        { }
    }
}
