﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Queries;
using AsgardMarketplace.Models.Items;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Items
{
    public class ItemListQueryHandler : IQueryHandler<string, List<ItemListModel>>
    {
        private readonly IQuery<string, ItemListModel> query;

        public ItemListQueryHandler(IQuery<string, ItemListModel> query)
        {
            this.query = query;
        }

        public async Task<List<ItemListModel>> Execute(string param)
        {
            return await query.ToList(param);
        }
    }
}
