﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Domain;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Items;

namespace AsgardMarketplace.CommandHandlers.Items.Save
{
    public class ItemSaveHandler : SaveHandler<ItemModel, Item>
    {
        public ItemSaveHandler(IRepository<Item> repository, IMapper<ItemModel, Item> mapper)
            : base(repository, mapper)
        { }
    }
}
