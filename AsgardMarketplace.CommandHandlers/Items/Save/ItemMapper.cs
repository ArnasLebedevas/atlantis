﻿using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Items;

namespace AsgardMarketplace.CommandHandlers.Items.Save
{
    public class ItemMapper : IMapper<ItemModel, Item>
    {
        public void Map(ItemModel model, Item entity)
        {
            entity.ID = model.ID;
            entity.Name = model.Name;
            entity.Price = model.Price;
            entity.Description = model.Description;
            entity.ImagePath = model.ImagePath;
        }
    }
}
