﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Queries;
using AsgardMarketplace.Models.Deliverings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.CommandHandlers.Deliverings
{
    public class DeliveringListQueryHandler : IQueryHandler<DeliveringItemParam, List<DeliveringItemListModel>>
    {
        private readonly IQuery<DeliveringItemParam, DeliveringItemListModel> query;

        public DeliveringListQueryHandler(IQuery<DeliveringItemParam, DeliveringItemListModel> query)
        {
            this.query = query;
        }

        public async Task<List<DeliveringItemListModel>> Execute(DeliveringItemParam param)
        {
            return await query.ToList(param);
        }
    }
}
