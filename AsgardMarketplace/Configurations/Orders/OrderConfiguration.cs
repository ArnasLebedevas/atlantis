﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.CommandHandlers.Orders;
using AsgardMarketplace.CommandHandlers.Orders.Delete;
using AsgardMarketplace.CommandHandlers.Orders.Save;
using AsgardMarketplace.Data.Core.Context;
using AsgardMarketplace.Data.Core.Orders;
using AsgardMarketplace.Data.Orders;
using AsgardMarketplace.Domain.Orders;
using AsgardMarketplace.Infrastructure;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Orders;
using System.Collections.Generic;

namespace AsgardMarketplace.Configurations.Orders
{
    public class OrderConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
               .AddCommandHandler<DeleteCommand<OrderModel>, OrderDeleteHandler>();

            services
               .AddCommandHandler<OrderModel, OrderSaveHandler>()
               .AddRepository<Order, DataRepository<Order>>()
               .AddMapper<OrderModel, Order, OrderMapper>();

            services
               .AddQuery<int, OrderModel, OrderQuery>()
               .AddQueryHandler<int, OrderModel, SingleHandler<int, OrderModel>>();

            services
               .AddQuery<string, OrderListModel, OrderListQuery>()
               .AddQueryHandler<string, List<OrderListModel>, OrderListQueryHandler>();

            services.AddTransient<IOrderRepository, OrderRepository>();
        }
    }
}
