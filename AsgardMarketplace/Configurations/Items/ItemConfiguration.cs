﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.CommandHandlers.Items;
using AsgardMarketplace.CommandHandlers.Items.Delete;
using AsgardMarketplace.CommandHandlers.Items.Save;
using AsgardMarketplace.Data.Core.Context;
using AsgardMarketplace.Data.Items;
using AsgardMarketplace.Domain.Items;
using AsgardMarketplace.Infrastructure;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Items;
using System.Collections.Generic;

namespace AsgardMarketplace.Configurations.Items
{
    public class ItemConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
                .AddCommandHandler<DeleteCommand<ItemModel>, ItemDeleteHandler>();

            services
                .AddCommandHandler<ItemModel, ItemSaveHandler>()
                .AddRepository<Item, DataRepository<Item>>()
                .AddMapper<ItemModel, Item, ItemMapper>();

            services
                .AddQuery<int, ItemModel, ItemQuery>()
                .AddQueryHandler<int, ItemModel, SingleHandler<int, ItemModel>>();

            services
               .AddQuery<string, ItemListModel, ItemListQuery>()
               .AddQueryHandler<string, List<ItemListModel>, ItemListQueryHandler>();
        }
    }
}
