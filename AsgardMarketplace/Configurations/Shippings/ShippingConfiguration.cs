﻿using AsgardMarketplace.CommandHandlers.Shippings;
using AsgardMarketplace.Data.Shippings;
using AsgardMarketplace.Infrastructure;
using AsgardMarketplace.Models.Shippings;
using System.Collections.Generic;

namespace AsgardMarketplace.Configurations.Shippings
{
    public class ShippingConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
                .AddQuery<ShippingItemParam, ShippingItemListModel, ShippingListQuery>()
                .AddQueryHandler<ShippingItemParam, List<ShippingItemListModel>, ShippingListQueryHandler>();
        }
    }
}
