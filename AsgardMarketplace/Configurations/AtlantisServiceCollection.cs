﻿using AsgardMarketplace.Configurations.Deliverings;
using AsgardMarketplace.Configurations.Items;
using AsgardMarketplace.Configurations.Orders;
using AsgardMarketplace.Configurations.Payments;
using AsgardMarketplace.Configurations.Shippings;
using AsgardMarketplace.Infrastructure;

namespace AsgardMarketplace.Configurations
{
    public class AtlantisServiceCollection : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services.Add(new ItemConfiguration());
            services.Add(new OrderConfiguration());
            services.Add(new ShippingConfiguration());
            services.Add(new PaymentConfiguration());
            services.Add(new DeliveringConfiguration());
        }
    }
}
