﻿using AsgardMarketplace.CommandHandlers.Payments.Save;
using AsgardMarketplace.Data.Core.Context;
using AsgardMarketplace.Data.Core.Payments;
using AsgardMarketplace.Domain.Payments;
using AsgardMarketplace.Infrastructure;
using AsgardMarketplace.Models.Payments;

namespace AsgardMarketplace.Configurations.Payments
{
    public class PaymentConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
                .AddCommandHandler<PaymentModel, PaymentSaveHandler>()
                .AddRepository<Payment, DataRepository<Payment>>()
                .AddMapper<PaymentModel, Payment, PaymentMapper>();

            services.AddTransient<IPaymentRepository, PaymentRepository>();
        }
    }
}
