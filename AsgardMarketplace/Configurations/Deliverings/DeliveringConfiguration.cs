﻿using AsgardMarketplace.CommandHandlers.Deliverings;
using AsgardMarketplace.Data.Deliverings;
using AsgardMarketplace.Infrastructure;
using AsgardMarketplace.Models.Deliverings;
using System.Collections.Generic;

namespace AsgardMarketplace.Configurations.Deliverings
{
    public class DeliveringConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
                .AddQuery<DeliveringItemParam, DeliveringItemListModel, DeliveringListQuery>()
                .AddQueryHandler<DeliveringItemParam, List<DeliveringItemListModel>, DeliveringListQueryHandler>();
        }
    }
}
