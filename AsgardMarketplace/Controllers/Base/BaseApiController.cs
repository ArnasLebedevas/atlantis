﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Base.Params;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Controllers.Base
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseApiController<TModel, TListModel> : ControllerBase
    {
        private readonly IBus mediator;

        public BaseApiController(IBus mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Save(TModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var saveResult = await mediator.Send(model);

            if (!saveResult.IsValid)
            {
                return BadRequest(saveResult);
            }

            return Created("", model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var command = new DeleteCommand<TModel>(id);

            var deleteResult = await mediator.Send(command);

            if (!deleteResult.IsValid)
            {
                return BadRequest(deleteResult);
            }

            return NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await mediator.Query<string, List<TListModel>>(string.Empty);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            if (id == 0)
            {
                var result = await mediator.Query<DefaultParam, TModel>(new DefaultParam());

                return Ok(result);
            }
            else
            {
                var result = await mediator.Query<int, TModel>(id);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromBody] TModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await mediator.Send(model);

            if (!result.IsValid)
            {
                return BadRequest(result);
            }

            return Ok(model);
        }
    }
}
