﻿using AsgardMarketplace.Controllers.Base;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Orders;

namespace AsgardMarketplace.Controllers
{
    public class OrdersController : BaseApiController<OrderModel, OrderListModel>
    {
        public OrdersController(IBus mediator) : base(mediator)
        { }
    }
}
