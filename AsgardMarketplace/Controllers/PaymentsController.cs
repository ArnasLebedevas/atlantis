﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Payments;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly IBus mediator;

        public PaymentsController(IBus mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Save(PaymentModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var saveResult = await mediator.Send(model);

            if (!saveResult.IsValid)
            {
                return BadRequest(saveResult);
            }

            return Created("", model);
        }
    }
}
