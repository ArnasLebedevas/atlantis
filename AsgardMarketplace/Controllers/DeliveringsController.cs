﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Deliverings;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveringsController : ControllerBase
    {
        private readonly IBus mediator;

        public DeliveringsController(IBus mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await mediator.Query<DeliveringItemParam, List<DeliveringItemListModel>>(new DeliveringItemParam());

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
