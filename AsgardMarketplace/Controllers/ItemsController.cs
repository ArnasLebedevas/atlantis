﻿using AsgardMarketplace.Controllers.Base;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Items;

namespace AsgardMarketplace.Controllers
{
    public class ItemsController : BaseApiController<ItemModel, ItemListModel>
    {
        public ItemsController(IBus mediator) : base(mediator)
        { }
    }
}
