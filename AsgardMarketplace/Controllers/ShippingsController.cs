﻿using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Shippings;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShippingsController : ControllerBase
    {
        private readonly IBus mediator;

        public ShippingsController(IBus mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await mediator.Query<ShippingItemParam, List<ShippingItemListModel>>(new ShippingItemParam());

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
