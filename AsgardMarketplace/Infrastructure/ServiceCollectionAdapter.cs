﻿using AsgardMarketplace.Domain;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Base.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace AsgardMarketplace.Infrastructure
{
    public class ServiceCollectionAdapter : IServiceCollection
    {
        private readonly Microsoft.Extensions.DependencyInjection.IServiceCollection serviceCollection;

        public ServiceCollectionAdapter(Microsoft.Extensions.DependencyInjection.IServiceCollection serviceCollection)
        {
            this.serviceCollection = serviceCollection;
        }

        public void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            serviceCollection.AddTransient<TService, TImplementation>();
        }

        IServiceCollection IServiceCollection.AddCommandHandler<TCommand, THandler>()
        {
            AddTransient<ICommandHandler<TCommand>, THandler>();

            return this;
        }

        IServiceCollection IServiceCollection.AddMapper<TModel, TEntity, TMapper>()
        {
            AddTransient<IMapper<TModel, TEntity>, TMapper>();

            return this;
        }

        IServiceCollection IServiceCollection.AddQuery<TParam, TResult, TQuery>()
        {
            AddTransient<IQuery<TParam, TResult>, TQuery>();

            return this;
        }

        IServiceCollection IServiceCollection.AddQueryHandler<TParam, TResult, TQueryHandler>()
        {
            AddTransient<IQueryHandler<TParam, TResult>, TQueryHandler>();

            return this;
        }

        IServiceCollection IServiceCollection.AddRepository<TEntity, TRepository>()
        {
            AddTransient<IRepository<TEntity>, TRepository>();

            return this;
        }

        IServiceCollection IServiceCollection.AddRule<TCommand, TRule>()
        {
            AddTransient<IRule<TCommand>, TRule>();

            return this;
        }
    }
}
