﻿using AsgardMarketplace.Models.Base;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AsgardMarketplace.Infrastructure
{
    public class DependencyResolver : IDependencyResolver
    {
        private readonly IServiceProvider serviceProvider;

        public DependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public T Resolve<T>()
        {
            return serviceProvider.GetRequiredService<T>();
        }
    }
}
