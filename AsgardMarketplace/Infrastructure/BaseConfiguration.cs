﻿using AsgardMarketplace.CommandHandlers.Base;
using AsgardMarketplace.Data.Core.Context.Infrastructure;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Base.Infrastructure;

namespace AsgardMarketplace.Infrastructure
{
    public class BaseConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services.AddTransient<IBus, Bus>();
            services.AddTransient<IDependencyResolver, DependencyResolver>();
            services.AddTransient<IValidator, Validator>();
            services.AddTransient<IWriteContext, WriteContextAdapter>();
            services.AddTransient<IReadContext, ReadContextAdapter>();
        }
    }
}
