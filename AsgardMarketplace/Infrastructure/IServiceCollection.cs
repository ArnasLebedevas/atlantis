﻿using AsgardMarketplace.Domain;
using AsgardMarketplace.Models.Base;
using AsgardMarketplace.Models.Base.Commands;
using AsgardMarketplace.Models.Base.Queries;

namespace AsgardMarketplace.Infrastructure
{
    public interface IServiceCollection
    {
        void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService;

        IServiceCollection AddQueryHandler<TParam, TResult, TImplementation>()
            where TImplementation : class, IQueryHandler<TParam, TResult>;

        IServiceCollection AddQuery<TParam, TResult, TImplementation>()
            where TImplementation : class, IQuery<TParam, TResult>;

        IServiceCollection AddCommandHandler<TCommand, THandler>()
            where THandler : class, ICommandHandler<TCommand>;

        IServiceCollection AddRepository<TEntity, TRepository>()
            where TRepository : class, IRepository<TEntity>;

        IServiceCollection AddMapper<TModel, TEntity, TMapper>()
            where TMapper : class, IMapper<TModel, TEntity>;

        IServiceCollection AddRule<TCommand, TRule>()
            where TRule : class, IRule<TCommand>;
    }
}
