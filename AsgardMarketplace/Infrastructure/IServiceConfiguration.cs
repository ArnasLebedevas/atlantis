﻿namespace AsgardMarketplace.Infrastructure
{
    public interface IServiceConfiguration
    {
        void Configure(IServiceCollection services);
    }

    public static class ServiceCollectionExtensions
    {
        public static void Add(this IServiceCollection services, IServiceConfiguration configuration)
        {
            configuration.Configure(services);
        }
    }
}
